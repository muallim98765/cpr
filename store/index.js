export const state = () => ({
  auth: false,
  bannerList: [],
  menuList: [],
  newsList: [],
  partnerList: [],
  productsList: [],
  instructionsList: [],
  managementsList: [],
  advantagesList: [],
  numbersList: [],
  contactsList: [],
  galleryVideoList: [],
  galleryPhotoList: [],
  regionList: [],
  apparatsList: [],
});
export const getters = {
  bannerList: (state) => {
    return state.bannerList;
  },
  menuList: (state) => {
    return state.menuList;
  },
  newsList: (state) => {
    return state.newsList;
  },
  partnerList: (state) => {
    return state.partnerList;
  },
  productsList: (state) => {
    return state.productsList;
  },
  instructionsList: (state) => {
    return state.instructionsList;
  },
  managementsList: (state) => {
    return state.managementsList;
  },
  advantagesList: (state) => {
    return state.advantagesList;
  },
  numbersList: (state) => {
    return state.numbersList;
  },
  contactsList: (state) => {
    return state.contactsList;
  },
  galleryVideoList: (state) => {
    return state.galleryVideoList;
  },
  galleryPhotoList: (state) => {
    return state.galleryPhotoList;
  },
  regionList: (state) => {
    return state.regionList;
  },
  apparatsList: (state) => {
    return state.apparatsList;
  },
};

export const mutations = {
  setState(state, { key, payload }) {
    state[key] = payload;
  },
};
export const actions = {
  fetchListDefault({ commit }, { api, key, params }) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.$axios.get(`/${api}`, { params });
        const { status, data, message } = res.data;
        if (status) {
          commit("setState", {
            key,
            payload: data.results || data || res || [],
          });
          resolve({
            status,
            data,
          });
        } else {
          reject(message);
        }
      } catch (error) {
        reject(error);
      }
    });
  },
};
