let ENV = {
  API_URL: process.env.API_URL,
  APP_BASE_URL: process.env.APP_BASE_URL,
  site_name: process.env.SITE_NAME,
  APP_BASE_IMG: {
    PATH: `${process.env.APP_BASE_URL}/logo_og.jpg`,
    WIDTH: "1200",
    HEIGHT: "630",
  },
};

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "template",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
      { name: "canonical", content: ENV.APP_BASE_URL },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon2.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["ant-design-vue/dist/antd.css", "@/assets/scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "@/plugins/antd-ui",
    {
      src: "~/plugins/VueAwesomeSwiper.js",
      ssr: false,
    },
    {
      src: "~/plugins/VueCoolLightbox.js",
      ssr: false,
    },
    { src: "~/plugins/axios.js", mode: "client" },
    { src: "~/plugins/global.js", mode: "client" },
    {
      src: "~/plugins/aos.js",
      ssr: false,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/svg", "@nuxtjs/i18n", "@nuxtjs/axios"],
  i18n: {
    locales: ["ru", "uz", "oz"],
    defaultLocale: "ru",
    vueI18n: {
      fallbackLocale: "ru",
      messages: {
        ru: {
          welcome: "Welcome",
        },
        uz: {
          welcome: "Bienvenue",
        },
        oz: {
          welcome: "Bienvenido",
        },
      },
    },
  },
  axios: {
    baseUrl: process.env.API_URL || "https://uniconsoftapi.mdg.uz/api/v1",
  },
  publicRuntimeConfig: {
    APP_BASE_URL:
      process.env.APP_BASE_URL || "https://uniconsoftapi.mdg.uz/api/v1",
    SITE_NAME: process.env.SITE_NAME,
  },
  //Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
